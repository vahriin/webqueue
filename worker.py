import asyncio

from uuid import UUID
from datetime import datetime
from aiohttp.web import Application

from typing import Type, Callable

from task import Task, TaskStatus


class Worker:
    """
    Base class for workers
    """

    async def run(self, app: Application):
        """
        this function will be run when the application is running
        """

        raise NotImplementedError()


class TaskWorker(Worker):
    """
    Worker, that execution Task
    """

    async def run(self, app: Application):
        while True:
            task_id: UUID = await app["queues"].worker_queue.get()

            # if KeyError raises here it is problem in application logic
            # let the application fall
            task: Task = app["tasks"][task_id]

            task.status = TaskStatus.Run
            task.start_time = datetime.now()

            await self.imitate_work(task.time_to_execute)

            task.status = TaskStatus.Completed
            task.exec_time = datetime.now()

            app["queues"].worker_queue.task_done()
            await app["queues"].db_queue.put(task_id)

    async def imitate_work(self, seconds: int):
        await asyncio.sleep(seconds)


class RedisWorker(Worker):
    """
    Worker that put Tasks into Redis
    """

    async def run(self, app: Application):
        while True:
            task_id: UUID = await app["queues"].db_queue.get()
            task: Task = app["tasks"][task_id]

            # if the database is unvailable let the application fall
            await app["redis"].hmset_dict(str(task.id), task.database_info)
            app["queues"].db_queue.task_done()
            app["tasks"].pop(task_id)


def add_worker(worker_type: Type[Worker]) -> Callable[[Type[Worker]], Application]:
    """Add worker to the application.
    
    Arguments:
        worker_type {Type[Worker]} -- Type of created worker.
    
    Returns:
        {Callable[[Type[Worker]], Application]} -- function that add worker. Must be coroutine
    """

    async def run_worker(app):
        app["workers"].append(app.loop.create_task(worker_type().run(app)))

    return run_worker


async def stop_workers(app):
    for worker in app["workers"]:
        worker.cancel()
        await worker
