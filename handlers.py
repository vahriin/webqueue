import uuid
from aiohttp import web

from typing import Optional

from task import Task


async def info(request: web.Request) -> web.Response:
    """
    Handle GET requests to /{id}
    """
    try:
        id_ = uuid.UUID(request.match_info["id"])
        task: Optional[Task] = request.app["tasks"].get(id_, None)
        if task:  # found in tasks storage
            return web.json_response(task.api_status)
        elif await request.app["redis"].exists(str(id_)):  # try to find in redis
            task = Task.from_database(
                id_, **(await request.app["redis"].hgetall(str(id_), encoding="utf-8"))
            )
            return web.json_response(task.api_status)
        else:
            raise web.HTTPNotFound(text="Task with this id is not exists")
    except ValueError:
        raise web.HTTPBadRequest(text="Passed parameter is not valid UUID")
    except OSError:
        raise web.HTTPInternalServerError(text="Could not connect to database")


async def new_task(request: web.Request):
    """
    Handle GET to /
    Create task and put it into queue
    """
    tasks_storage = request.app["tasks"]
    task = Task()
    tasks_storage[task.id] = task
    await request.app["queues"].worker_queue.put(task.id)
    return web.json_response(task.id_dict)
