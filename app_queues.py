import asyncio


class AppQueues:
    def __init__(self):
        self.worker_queue = asyncio.Queue()
        self.db_queue = asyncio.Queue()
