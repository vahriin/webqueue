import uuid
from random import randint
from datetime import datetime
from enum import Enum

from typing import Optional


class TaskStatus(Enum):
    InQueue = "In Queue"
    Run = "Run"
    Completed = "Completed"


class Task:
    def __init__(self):
        self.id: uuid.UUID = uuid.uuid4()
        self.status: TaskStatus = TaskStatus.InQueue
        self.time_to_execute: int = randint(0, 10)
        self.create_time: datetime = datetime.now()
        self.start_time: Optional[datetime] = None
        self.exec_time: Optional[datetime] = None

    @staticmethod
    def from_database(id_: uuid.UUID, **kwargs):
        """
        Create task from redis information
        """
        t = Task()
        t.id = id_
        t.status = TaskStatus.Completed
        t.create_time = datetime.fromisoformat(kwargs["create_time"])
        t.start_time = datetime.fromisoformat(kwargs["start_time"])
        t.exec_time = datetime.fromisoformat(kwargs["exec_time"])
        t.time_to_execute = int((t.exec_time - t.start_time).total_seconds())
        return t

    @property
    def api_status(self):
        """
        Return representation for API call
        """
        return {
            "status": self.status.value,
            "create_time": self.create_time.isoformat(timespec="seconds"),
            "start_time": self.start_time.isoformat(timespec="seconds") if self.start_time else None,
            "time_to_execute": str(self.time_to_execute),
        }

    @property
    def id_dict(self):
        """
        Return representation of id. Used to return to the user
        """
        return {"id": str(self.id)}

    @property
    def database_info(self):
        """
        Return representation for send to redis
        """
        return {
            "create_time": self.create_time.isoformat(timespec="seconds"),
            "start_time": self.start_time.isoformat(timespec="seconds"),
            "exec_time": self.exec_time.isoformat(timespec="seconds"),
        }
