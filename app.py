import json

import aioredis

from aiohttp import web
from typing import Dict, Tuple, Optional

from app_queues import AppQueues
from handlers import info, new_task
from worker import TaskWorker, RedisWorker, add_worker, stop_workers


def main():
    app = create_app(*read_config())
    web.run_app(app)


def read_config(config: str = "config.json") -> Tuple[Dict[str, str], Dict[str, int]]:
    config_file = json.load(open(config))
    redis_conf = config_file["redis"]
    workers_conf = config_file["workers"]
    return redis_conf, workers_conf


def create_app(redis: Dict[str, str], workers: Dict[str, int]) -> web.Application:
    app = web.Application()

    app["tasks"] = {}  # Dict[UUID, Task]
    app["queues"] = AppQueues()
    app["workers"] = []  # List[Worker]
    app["redis"] = None

    app.add_routes([web.get("/", new_task), web.get("/{id}", info)])

    workers_amount: int = sum(workers.values())

    # connect on startup
    app.on_startup.append(
        connect_redis(redis["host"], redis["password"], max_conn=workers_amount)
    )

    # add task workers
    app.on_startup.extend(
        [add_worker(TaskWorker) for _ in range(workers.get("task_workers", 0))]
    )

    # add redis workers
    app.on_startup.extend(
        [add_worker(RedisWorker) for _ in range(workers.get("redis_workers", 0))]
    )

    # stop workers while shutdonw
    app.on_cleanup.append(stop_workers)

    return app


def connect_redis(address: str, password: Optional[str] = None, max_conn: int = 10):
    async def connect(app):
        app["redis"] = aioredis.Redis(
            await aioredis.create_pool(address, password=password, maxsize=max_conn)
        )

    return connect


if __name__ == "__main__":
    main()
